# About

* Simple device which just watch level change on predefined pin.
* This allows to track LED blinking for instance, when human eye is not good enough, but logic analyzer is not suitable either (consume a lot of RAM for example).
* Device prints on UART only when GPIO logic value change, so even processing is easy.
* Based on ESP32 MCU.
* Input pin: `GPIO4`
* Output: standard ESP log
* TODO: Add example log