/* GPIO Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <esp_log.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "driver/gpio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/task.h"

/**
 * Brief:
 * This test code shows how to configure gpio and how to use gpio interrupt.
 *
 * GPIO4:  input, pulled up, interrupt from rising edge and falling edge
 *
 */
#define GPIO_INPUT_IO_0 4
#define GPIO_INPUT_PIN_SEL (1ULL << GPIO_INPUT_IO_0)
#define ESP_INTR_FLAG_DEFAULT 0

#define SEPARATOR_WHEN_MORE_THAN_MS (5000)

static xQueueHandle gpio_evt_queue = NULL;

static void IRAM_ATTR gpio_isr_handler(void* arg) {
  uint32_t gpio_num = (uint32_t)arg;
  xQueueSendFromISR(gpio_evt_queue, &gpio_num, NULL);
}

static void gpio_task_example(void* arg) {
  uint32_t io_num;

  // By default is pull-up enabled - set as default value
  int prev_level = 1;
  int current_level = 1;

  // Keep track about time when there was change
  uint32_t current_time_ms = esp_log_timestamp();
  uint32_t prev_change_ms = current_time_ms;

  // Difference between previous and current time
  uint32_t time_diff_ms;

  for (;;) {
    if (xQueueReceive(gpio_evt_queue, &io_num, portMAX_DELAY)) {
      current_level = gpio_get_level(io_num);

      current_time_ms = esp_log_timestamp();

      time_diff_ms = current_time_ms - prev_change_ms;

      if (time_diff_ms >= SEPARATOR_WHEN_MORE_THAN_MS) {
        printf("\n\n");
      }

      if (current_level != prev_level) {
        if (current_level) {
          ESP_LOGI("GPIO", "%d (%d)   __|\"\"    %d ms", io_num, current_level,
                   time_diff_ms);
        } else {
          ESP_LOGI("GPIO", "%d (%d)   \"\"|__    %d ms", io_num, current_level,
                   time_diff_ms);
        }

      } else {
        // Not much to do
      }

      prev_level = current_level;
      prev_change_ms = current_time_ms;
    }
  }
}

void app_main(void) {
  gpio_config_t io_conf;

  // interrupt of rising edge
  io_conf.intr_type = GPIO_INTR_ANYEDGE;
  // bit mask of the pins, use GPIO4/5 here
  io_conf.pin_bit_mask = GPIO_INPUT_PIN_SEL;
  // set as input mode
  io_conf.mode = GPIO_MODE_INPUT;
  // enable pull-up mode
  io_conf.pull_up_en = 1;

  // disable pull-down mode
  io_conf.pull_down_en = 0;

  gpio_config(&io_conf);

  // create a queue to handle gpio event from isr
  gpio_evt_queue = xQueueCreate(10, sizeof(uint32_t));
  // start gpio task
  xTaskCreate(gpio_task_example, "gpio_task_example", 2048, NULL, 10, NULL);

  // install gpio isr service
  gpio_install_isr_service(ESP_INTR_FLAG_DEFAULT);
  // hook isr handler for specific gpio pin
  gpio_isr_handler_add(GPIO_INPUT_IO_0, gpio_isr_handler,
                       (void*)GPIO_INPUT_IO_0);

  printf("Minimum free heap size: %d bytes\n",
         esp_get_minimum_free_heap_size());
}
